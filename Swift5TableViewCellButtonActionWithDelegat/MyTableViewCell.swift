////  MyTableViewCell.swift
//  Swift5TableViewCellButtonActionWithDelegat
//
//  Created on 01/11/2020.
//  
//

import UIKit

// 1.
protocol MyTableViewCellDelegate: AnyObject {
    func didButtonTap(with title: String)
}

class MyTableViewCell: UITableViewCell {

    static let identifier = "MyTableViewCell"
    
    @IBOutlet weak var myButton: UIButton!
    
    private var title: String = ""
    
    // 2.
    weak var delegate: MyTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    static func nib() -> UINib {
        return UINib(nibName: "MyTableViewCell", bundle: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func didButtonTap(_ sender: Any) {
        // 3.
        delegate?.didButtonTap(with: title)
    }
    
    func configure(with title: String) {
        // 4.
        self.title = title
        myButton.setTitle(title, for: .normal)
    }
}

