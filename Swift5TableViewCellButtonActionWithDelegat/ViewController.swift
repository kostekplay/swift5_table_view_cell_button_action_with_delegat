////  ViewController.swift
//  Swift5TableViewCellButtonActionWithDelegat
//
//  Created on 01/11/2020.
//  
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let models = [
        "Audi",
        "Volvo",
        "BMW"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(MyTableViewCell.nib(), forCellReuseIdentifier: MyTableViewCell.identifier)
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyTableViewCell.identifier, for: indexPath) as! MyTableViewCell
        // 5.
        cell.delegate = self
        cell.configure(with: models[indexPath.row])
        return cell
    }
}

// 6.
extension ViewController: MyTableViewCellDelegate {
    // 7.
    func didButtonTap(with title: String) {
        print("Tapped : \(title)")
    }
}

